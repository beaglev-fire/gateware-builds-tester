# Gateware Builds Tester

## Description
Internal project intended for CI of the BeagleV-Fire gateware.

This script builds a set of bitstreams to test the ability to build gateware with different build option combinations (cape, MIPI connector, high-speed connector FPGA logic) and different bitstream component git branches.

## Installation
This script requires some Python packages to be installed:

```
pip3 install gitpython
pip3 install pyyaml
pip3 install junit_xml
```

## Usage
Usage:
```
python3 bitstream-builds-tester.py <BUILD_OPTIONS_DIR>
```
Where:
- <BUILD_OPTIONS_DIR> is the directory containing the YAML files describing gateware build options to build. This is typically either "build_options" or "custom-fpga-design".

Example:
```
python3 bitstream-builds-tester.py https://openbeagle.org/beaglev-fire/gateware build_options
```
